package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        public String attack() {
                return "Sword";
        }

        public String getType(){
                return "Sword";
        }
}
