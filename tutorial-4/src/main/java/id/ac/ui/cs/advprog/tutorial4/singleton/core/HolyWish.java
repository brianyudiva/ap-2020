package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    //using eager singleton

    private static HolyWish obj  = new HolyWish();

    private String wish;

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    public static HolyWish getInstance(){
        return obj;
    }

    @Override
    public String toString() {
        return wish;
    }
}
