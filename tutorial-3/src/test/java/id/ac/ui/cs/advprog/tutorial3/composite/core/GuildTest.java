package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        int jumlahChild = guild.getMemberList().size();
        Member test1 = new OrdinaryMember("test1", "test");
        guild.addMember(guildMaster, test1);
        assertEquals(guild.getMemberList().size(), jumlahChild + 1);
    }

    @Test
    public void testMethodRemoveMember() {
        int jumlahChild = guild.getMemberList().size();
        Member test1 = new OrdinaryMember("test1", "test");
        guild.addMember(guildMaster, test1);
        guild.removeMember(guildMaster, test1);
        assertEquals(guild.getMemberList().size(), jumlahChild);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(sasuke, guild.getMember("Asep", "Servant"));
    }
}
