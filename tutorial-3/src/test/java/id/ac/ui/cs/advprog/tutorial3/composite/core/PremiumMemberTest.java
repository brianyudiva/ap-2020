package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member test1 = new PremiumMember("test1", "test11");
        member.addChildMember(test1);
        assertEquals(member.getChildMembers().size(), 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member test1 = new PremiumMember("test1", "test11");
        member.addChildMember(test1);
        assertEquals(member.getChildMembers().size(), 1);
        member.removeChildMember(test1);
        assertEquals(member.getChildMembers().size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member test1 = new PremiumMember("test1", "test11");
        member.addChildMember(test1);
        member.addChildMember(test1);
        member.addChildMember(test1);
        member.addChildMember(test1);
        assertEquals(member.getChildMembers().size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("Master1", "Master");
        Member test1 = new PremiumMember("test1", "test11");
        master.addChildMember(test1);
        master.addChildMember(test1);
        master.addChildMember(test1);
        master.addChildMember(test1);
        assertEquals(master.getChildMembers().size(), 4);
    }
}
