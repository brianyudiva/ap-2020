package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    Random rndm;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.rndm = new Random();
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        return weapon.getWeaponValue() + rndm.nextInt(6);
    }

    @Override
    public String getDescription() {
        return weapon.getDescription();
    }
}
