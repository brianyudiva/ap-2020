package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        public Longbow(){
                
        super.weaponName = "Longbow";
        super.weaponDescription = "Big Longbow";
        super.weaponValue = 15;
        }
}
