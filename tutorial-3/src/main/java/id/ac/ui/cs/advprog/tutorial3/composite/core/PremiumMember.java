package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        String name;
        String role;
        ArrayList<Member> childMember;

        public PremiumMember(String name, String role){
                this.name = name;
                this.role = role;
                this.childMember = new ArrayList<Member>();
        }

        @Override
        public String getName() {
        return this.name;
        }

        @Override
        public String getRole() {
        return this.role;
        }

        @Override
        public void addChildMember(Member member) {
                if (this.childMember.size() < 3){
                        this.childMember.add(member);
                }else if (role.equals("Master")){
                        this.childMember.add(member);
                }
        }

        @Override
        public void removeChildMember(Member member) {
        this.childMember.remove(member);
        }

        @Override
        public List<Member> getChildMembers() {
        return this.childMember;
        }
                
}
