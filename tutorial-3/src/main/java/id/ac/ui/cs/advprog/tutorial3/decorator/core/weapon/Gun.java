package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
        public Gun(){
        super.weaponName = "Gun";
        super.weaponDescription = "Automatic Gun";
        super.weaponValue = 20;
        }
}
