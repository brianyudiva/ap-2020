package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        public Sword(){
                
        super.weaponName = "Sword";
        super.weaponDescription = "Great Sword";
        super.weaponValue = 25;
        }
}
