package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    Random rndm;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
        rndm = new Random();
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        return weapon.getWeaponValue() + rndm.nextInt(6) + 10;
    }

    @Override
    public String getDescription() {
        return weapon.getDescription();
    }
}
