package id.ac.ui.cs.advprog.tutorial5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class Tutorial5Application {
    public static void main(String[] args) {
        run(Tutorial5Application.class, args);
    }
}
