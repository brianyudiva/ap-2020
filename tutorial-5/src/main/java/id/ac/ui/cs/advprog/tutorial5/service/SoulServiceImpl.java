package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    private SoulRepository repo;

    @Override
    public List<Soul> findAll() {
        return repo.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return repo.findById(id);
    }

    @Override
    public void erase(Long id) {
        repo.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        repo.save(soul);
        return soul;
    }

    @Override
    public Soul register(Soul soul) {
        repo.save(soul);
        return soul;
    }
}

